# Raspberry Fan Control Service

A simple service which automatically enables and disables a fan connected to a Raspberry (using GPIO).

## Install
To install it you have to:
- copy files "fancontrol" and "fancontrol.conf" in a folder (I used /root/fancontrol/)
- change the files "fancontrol" and "filecontrol.service" writing your installation path
- copy "fancontrol.service" in /etc/init.d/ as fancontrol:
```bash
$ sudo cp /root/fancontrol/fancontrol.service /etc/init.d/fancontrol
```
- enable your service:
```bash
$ sudo systemctl enable fancontrol
$ sudo systemctl start fancontrol
```
